$( document ).ready(function(){
	var csrftoken = Cookies.get('csrftoken');
	
	
	
	$('[data-toggle="tooltip"]').tooltip();
	$( "#deadline" ).datepicker({ dateFormat: 'dd/mm/yy' });
	$( "#edit_deadline" ).datepicker({ dateFormat: 'dd/mm/yy' });
	
	$(".notes").click(function() {
		var task_id = $(this).closest('tr').attr('id');
		
		$.get("get_notes/", {note:task_id}).done(function(data){
			swal({
				title: "Task: #" + task_id,
                text: data,
                html: true
			});
		});
	});

    $("#task_form").submit(function() {
    	var data = $("#task_form").serialize();
		
    	$.post("new_task/", data).done(function(){
    		window.location.href = "/tasks/"
    	});
    });
	
	$(".delete").click(function() {
		task_id = $(this).closest('tr').attr('id');
		
		swal({
			title: "Are you sure?",
			text: "Deleted task can not be recovered!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete task",
			cancelButtonText: "Cancel",
			closeOnConfirm: false,
			closeOnCancel: false
		  }, function(isConfirm) {
			if (isConfirm) {
				window.location.href = "delete_task/" + task_id
			}
			else {
				swal("Cancelled");
			}
		  });
	});
	
	$(".hide_stuff").click(function() {
		var complete;
		
		if ($(this).attr("hide_tasks") == 1) {
			complete = 1;
			$(this).text("Show All Tasks");
			$(this).attr("hide_tasks", "0");
		}
		else {
			complete = 0;
			$(this).text("Hide Complete Tasks");
			$(this).attr("hide_tasks", "1");
		}
		
		$('#task_table tbody tr').each(function() {
			if ($(this).attr("complete") == "1") {
				if (complete == 1) {
					$(this).hide();
				}
				else {
					$(this).show();
				}
			};
		});
	});
	
	$(".edit").click(function() {
		var task_id = $(this).closest('tr').attr('id');
		var task_name = $("#"+task_id).find(".name").text();
		var deadline = $("#"+task_id).find(".deadline").text();
		
		if (deadline == "No Date Set") {
			deadline = ""
		};

		$("#task_id").val(task_id);
		$("#task_name").val(task_name);
		$("#deadline").val(deadline);
		
		$.get("get_notes/", {note:task_id}).done(function(data){
			$("#task_notes").val(data);
		});
		
		
	});
})