from django.conf.urls import url
import views

urlpatterns = [
	url(r'^$', views.index, name="tasks"),
	url(r'new_task/$', views.new_task, name="new_task"),
	url(r'get_notes/$', views.get_notes, name="notes"),
	url(r'delete_task/(?P<task>\d+)$', views.delete_task, name="delete"),
	url(r'complete_task/(?P<task>\d+)/(?P<status>\d+)$', views.complete_task, name="complete"),
	]