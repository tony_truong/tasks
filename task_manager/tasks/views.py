from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from json import dumps
from django.contrib.auth.decorators import login_required

import datetime as d

from .models import Task

# def user_login(request):
# 	print "=========="
# 	# If the request is a HTTP POST, try to pull out the relevant information.
# 	if request.method == 'POST':
# 		# Gather the username and password provided by the user.
# 		# This information is obtained from the login form.
# 				# We use request.POST.get('<variable>') as opposed to request.POST['<variable>'],
# 				# because the request.POST.get('<variable>') returns None, if the value does not exist,
# 				# while the request.POST['<variable>'] will raise key error exception
# 		username = request.POST.get('username')
# 		password = request.POST.get('password')
# 		print username
# 		print "============================"
# 	
# 		# Use Django's machinery to attempt to see if the username/password
# 		# combination is valid - a User object is returned if it is.
# 		user = authenticate(username=username, password=password)
# 	
# 		# If we have a User object, the details are correct.
# 		# If None (Python's way of representing the absence of a value), no user
# 		# with matching credentials was found.
# 		if user:
# 			# Is the account active? It could have been disabled.
# 			if user.is_active:
# 				# If the account is valid and active, we can log the user in.
# 				# We'll send the user back to the homepage.
# 				login(request, user)
# 				return HttpResponseRedirect('/tasks/')
# 			else:
# 				# An inactive account was used - no logging in!
# 				return HttpResponse("Your Rango account is disabled.")
# 		else:
# 			# Bad login details were provided. So we can't log the user in.
# 			print "Invalid login details: {0}, {1}".format(username, password)
# 			return HttpResponse("Invalid login details supplied.")
# 	
# 	# The request is not a HTTP POST, so display the login form.
# 	# This scenario would most likely be a HTTP GET.
# 	else:
# 		# No context variables to pass to the template system, hence the
# 		# blank dictionary object...
# 		return render(request, 'tasks/login.html', {})
# 
# @login_required

@login_required
def index(request):
	tasks = Task.objects.all()
	context_dict = {"tasks": tasks}
	
	return render(request, 'tasks/index.html', context=context_dict)

def new_task(request):
	if request.method == "POST":
		data = request.POST

		if data["deadline"] != "":
			deadline = d.datetime.strptime(data["deadline"],'%d/%m/%Y').strftime('%Y-%m-%d')
		else:
			deadline = None

		if data["task_id"] == "":
			Task.objects.create(task_name=data["task_name"],deadline=deadline,notes=data["task_notes"])
		else:
			Task.objects.filter(id=int(data["task_id"])).update(task_name=data["task_name"],deadline=deadline,notes=data["task_notes"])

	return HttpResponse("Done")
	

def get_notes(request):
	task = Task.objects.get(id=int(request.GET["note"]))
	
	return HttpResponse(task.notes)
	
def delete_task(request,task):
	Task.objects.filter(id=int(task)).delete()
	
	return HttpResponseRedirect(reverse('tasks'))

def complete_task(request,task,status):
	if status == "1":
		complete_status = True
	else:
		complete_status = False
		
	Task.objects.filter(id=int(task)).update(complete=complete_status)
	
	return HttpResponseRedirect(reverse('tasks'))