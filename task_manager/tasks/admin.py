from django.contrib import admin

from models import Task

class TaskAdmin(admin.ModelAdmin):
	list_display = ["id","task_name","deadline","notes","complete"]
	search_fields = ["id","task_name"]

admin.site.register(Task,TaskAdmin)

# Register your models here.
