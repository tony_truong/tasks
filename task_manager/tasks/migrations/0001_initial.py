# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-30 20:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('task_name', models.CharField(max_length=258)),
                ('notes', models.TextField(blank=True, null=True)),
                ('complete', models.BooleanField(default=False)),
                ('deadline', models.DateField(blank=True, null=True)),
            ],
        ),
    ]
