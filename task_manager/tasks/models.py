from __future__ import unicode_literals

from django.db import models

import datetime

class Task(models.Model):
	task_name = models.CharField(max_length=258,blank=False)
	notes = models.TextField(null=True, blank=True)
	complete = models.BooleanField(default=False)
	deadline = models.DateField(blank=True, null=True)

	def __str__(self):
		return self.task_name