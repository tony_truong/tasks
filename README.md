# To Do App #

Simple to_do application. Allows you to create a task and set a deadline for that task.

Functions:
 * Edit task details
 * Mark task complete/not complete
 * Delete task
 * Add new task
 * Hide completed tasks

* Version 0.1


### How do I get set up? ###

* Django (1.9.7)

* Run migrations - python manage.py migrate
* Create user - python manage.py createsuperuser

### Who do I talk to? ###

* Tony (repo owner)